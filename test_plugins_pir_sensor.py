from plugins import pir_sensor

import threading
import time

if __name__ == "__main__":
    sensor = pir_sensor.PIRSensor()

    stop_event = threading.Event()
    sensor.set_stop_event(stop_event)
    
    motion_event = threading.Event()
    sensor.set_event(motion_event)
 
    sensor.initialize()
    
    start = time.time()
    while time.time()-start < 60:
        if motion_event.isSet():
            print "intruder"
    stop_event.set()
