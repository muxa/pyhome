import camera, executor

import threading
import time
from Queue import Queue
import telegram
from PIL import Image
import RPi.GPIO as GPIO
from core import restricted, user

import state, configuration

import logging

logger = logging.getLogger("pyhome")

STOP_EVENT = None

CAMERA = None
EXECUTOR = None
QUEUE = Queue(100)

def initialize(bot):
    global CAMERA
    # starting camera if it was started before
    if state.get_state("security", False):
        _init(bot)
        CAMERA.start_security()
    if state.get_state("stream", False):
        _init(bot)
        CAMERA.start_stream()


def _init(bot):
    global CAMERA, EXECUTOR, QUEUE
    if CAMERA is None:
        CAMERA = camera.SurveillanceCamera()
        CAMERA.set_command_queue(QUEUE)
        CAMERA.initialize()
    if EXECUTOR is None:
        EXECUTOR = executor.Executor()
        EXECUTOR.initialize(QUEUE, bot)

@restricted
def security(bot, update, args):
    global CAMERA
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if len(args) != 1:
        bot.send_message(chat_id=update.message.chat_id, text="Usage: /security {ON|OFF|on|off}")
        return
    arg = args[0].upper()
    if arg not in ["ON", "OFF"]:
        bot.send_message(chat_id=update.message.chat_id, text="Usage: /security {ON|OFF|on|off}")
        return
    if CAMERA is None and arg == "OFF":
        bot.send_message(chat_id=update.message.chat_id, text="Start security first with the /security ON command.")
        return
    _init(bot)
    message = "Something went wrong when processing /security query."
    if arg == "ON":
        CAMERA.start_security()
        message = "Security is enabled."
    else:
        CAMERA.stop_security()
        message = "Security is disabled."
    bot.send_message(chat_id=update.message.chat_id, text=message)

@restricted
def stream(bot, update, args):
    global CAMERA
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if len(args) != 1:
        bot.send_message(chat_id=update.message.chat_id, text="Usage: /stream {ON|OFF|on|off}")
        return
    arg = args[0].upper()
    if arg not in ["ON", "OFF"]:
        bot.send_message(chat_id=update.message.chat_id, text="Usage: /stream {ON|OFF|on|off}")
        return
    if CAMERA is None and arg == "OFF":
        bot.send_message(chat_id=update.message.chat_id, text="Start stream first with the /stream ON command.")
        return
    _init(bot)
    message = "Something went wrong when processing /stream query."
    if arg == "ON":
        CAMERA.start_stream()
        message = "Stream is enabled."
    else:
        CAMERA.stop_stream()
        message = "Stream is disabled."
    bot.send_message(chat_id=update.message.chat_id, text=message)

@restricted
def start_camera(bot, update):
    global STOP_EVENT
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if STOP_EVENT is not None and not STOP_EVENT.isSet():
        message = "The camera has not been already started. Use /stop_camera command before."
    else:
        cam = camera.SurveillanceCamera()
        if STOP_EVENT is not None:
            STOP_EVENT.clear()
        else:
            STOP_EVENT = threading.Event()
        cam.set_stop_event(STOP_EVENT)
    
        exe = executor.Executor()
        exe.set_stop_event(STOP_EVENT)

        queue = Queue(100)

        cam.set_command_queue(queue)
 
        exe.initialize(queue, bot)
        cam.initialize()
        message = "The camera has been started."
    bot.send_message(chat_id=update.message.chat_id, text=message)

@restricted
def stop_camera(bot, update):
    global STOP_EVENT
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if STOP_EVENT is None:
        message = "The camera has not been even started. Use /start_camera command before."
    else:
        STOP_EVENT.set()
        STOP_EVENT = None
        message = "The camera has been stopped."
    bot.send_message(chat_id=update.message.chat_id, text=message)


@user
def pic(bot, update, args):
    global CAMERA
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    _send_pic(bot, update)

def _send_pic(bot, update):
    _init(bot)
    with camera.SurveillanceCamera.output_pic.condition:
        camera.SurveillanceCamera.output_pic.condition.wait()
        stream_buf = camera.SurveillanceCamera.output_pic.buffer
        stream_buf.seek(0)
        image = Image.open(stream_buf)
        image.save('/home/pi/pyhome/pic.jpg')
    bot.send_photo(chat_id=update.message.chat_id, photo=open('/home/pi/pyhome/pic.jpg', 'rb'))

@user
def right(bot, update, args):
    angle = state.get_state("angle", 90) - 15
    if angle < 0:
        angle = 0
    GPIO.setmode(GPIO.BOARD)
    servo_pin = configuration.get_config("pinout", "Servo", 16)
    GPIO.setup(servo_pin, GPIO.OUT)
    pwm = GPIO.PWM(servo_pin, 50)
    pwm.start(0)
    pwm.ChangeDutyCycle(angle / 18 + 1)
    time.sleep(0.3)
    pwm.stop()
    time.sleep(0.5)
    state.set_state("angle", angle)
    _send_pic(bot, update)

@user
def left(bot, update, args):
    angle = state.get_state("angle", 90) + 15
    if angle > 190:
        angle = 190
    GPIO.setmode(GPIO.BOARD)
    servo_pin = configuration.get_config("pinout", "Servo", 16)
    GPIO.setup(servo_pin, GPIO.OUT)
    pwm = GPIO.PWM(servo_pin, 50)
    pwm.start(0)
    pwm.ChangeDutyCycle(angle / 18 + 1)
    time.sleep(0.3)
    pwm.stop()
    time.sleep(0.5)
    state.set_state("angle", angle)
    _send_pic(bot, update)

@user
def center(bot, update, args):
    angle = 90
    GPIO.setmode(GPIO.BOARD)
    servo_pin = configuration.get_config("pinout", "Servo", 16)
    GPIO.setup(servo_pin, GPIO.OUT)
    pwm = GPIO.PWM(servo_pin, 50)
    pwm.start(0)
    pwm.ChangeDutyCycle(angle / 18 + 1)
    time.sleep(0.3)
    pwm.stop()
    time.sleep(0.5)
    state.set_state("angle", angle)
    _send_pic(bot, update)

CMD = {"admin":[security, stream],
       "user": [pic, right, left, center],
       "all":[]}
