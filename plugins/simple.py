from core import restricted


@restricted
def rping(bot, update, args):
    bot.sendMessage(chat_id=update.message.chat_id, text="rpong")

def ping(bot, update, args):
    bot.sendMessage(chat_id=update.message.chat_id, text="pong")

def args(bot, update, args):
    text_caps = ' '.join(args).upper()
    bot.sendMessage(chat_id=update.message.chat_id, text=text_caps)

CMD = {"admin": [rping],
       "user": [],
       "all": [ping]}
