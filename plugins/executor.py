#!/usr/bin/python

from Queue import Queue
import threading
import subprocess
import dropbox
import os
import configuration

from os import listdir
from os.path import isfile, join

class TransferData:
    def __init__(self, access_token):
        self.access_token = access_token

    def upload_file(self, file_from, file_to):
        """upload a file to Dropbox using API v2
        """
        dbx = dropbox.Dropbox(self.access_token)

        with open(file_from, 'rb') as f:
            dbx.files_upload(f.read(), file_to)


class Command():
    def __init__(self, type, args):
        self.type = type
        self.args = args
    def cmd_type(self):
        return self.type
   
    def cmd_args(self):
        return self.args 

def dispatch_video(cmd):
    before = cmd.cmd_args()[0]
    after = cmd.cmd_args()[1]
    date = cmd.cmd_args()[2]
    fname = before.split("-before")[0]
        
    # concatenate files before and after movement
    subprocess.call("cat {} {} > {} && rm -f {}".format(before,after,fname+".h264",fname+"-*.h264"),shell=True)
    # convert the resulted file to a supersmall size to send via Telegram
    subprocess.call('ffmpeg -i {} -an -c:v libx264 -crf 26 -vf scale=320:-1 {}.mp4'.format(fname+".h264",fname+".h264"), shell=True)
    # the file must be uploaded to Telegram server, hence upload a new video file using multipart/form-data.
    if Executor.bot is not None:
        for recipient in configuration.get_config("admin", "admins", [])+configuration.get_config("admin", "users", []):
            subprocess.call('curl -v -F chat_id={} -F video=@{}.mp4 -F caption="{}" {}/sendVideo'.format(recipient, fname+".h264", "Motion detected: "+date, Executor.bot.base_url), shell=True)


    access_token = configuration.get_config("token", "dropbox", "")
    transferData = TransferData(access_token)

    # API v2
    transferData.upload_file(fname+".h264", '/rpimotion/'+fname.split("/")[-1]+".h264")
    # delete file
    os.remove(fname+".h264")
    os.remove(fname+".h264.mp4")
        
def dispatch(cmd):
    {"video": dispatch_video}[cmd.cmd_type()](cmd)

class Executor(object):
    thread = None
    queue = None
    bot = None
    stop_event = threading.Event()

    def initialize(self, queue, bot):
        # if there were files, copy them to dropbox, cleanup the directory
        path = configuration.get_config("security", "path", "/home/pi/motion/video")
        videos = [f for f in listdir(path) if isfile(join(path, f))]
        
        if bot is not None and len(videos) > 0:
            for admin in configuration.get_config("admin", "admins", []):
                bot.sendMessage(chat_id=admin, text="There are "+str(len(videos))+" videos unprocessed. Copy them to dropbox...")

        access_token = configuration.get_config("token", "dropbox", "")
        transferData = TransferData(access_token)

        for video in videos:
            transferData.upload_file(join(path, video), '/rpimotion/'+video)
            os.remove(join(path, video))

        if bot is not None and len(videos) > 0:
            for admin in configuration.get_config("admin", "admins", []):
                bot.sendMessage(chat_id=admin, text="Dropbox upload is done.")

        if Executor.thread is None:
            Executor.stop_event.clear()
            Executor.queue = queue
            Executor.bot = bot
            Executor.thread = threading.Thread(target=self._thread)
            Executor.thread.daemon = True
            Executor.thread.start()

    def set_stop_event(self, stop_event):
        Executor.stop_event = stop_event

    @classmethod
    def _thread(cls):
        while not cls.stop_event.isSet():
            cmd = cls.queue.get()
            cls.queue.task_done()
            if cls.bot is not None:
                for admin in configuration.get_config("admin", "admins", []):
                    cls.bot.sendMessage(chat_id=admin, text="Processing '"+cmd.cmd_type()+"' command. "+str(cls.queue.qsize())+" commands approximately remainig...")

            dispatch(cmd)
        while not cls.queue.empty():
            cmd = cls.queue.get()
            cls.queue.task_done()
            dispatch(cmd)
            #subprocess.call(cmd.get("command",""), shell=cmd.get("shell",False))

