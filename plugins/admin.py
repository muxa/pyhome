from functools import wraps
import logging
import configuration
import telegram
import subprocess
import time, os, sys
import psutil

from telegram.inlinekeyboardbutton import InlineKeyboardButton
from telegram.inlinekeyboardmarkup import InlineKeyboardMarkup

from core import restricted, user, build_menu

import core
import json

logger = logging.getLogger('pyhome')

@restricted
def scan(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    new_cmds = core.scan_plugins(bot)
    bot.send_message(chat_id=update.message.chat_id, text="Scanning complete. Loaded new commands:\n"+new_cmds)

@restricted
def auth(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if len(args) != 1:
        bot.send_message(chat_id=update.message.chat_id, text="Usage: /auth userd_id.")
        return
    arg = int(args[0])
    add_user(bot, arg)


def add_user(bot, user_id):
    if configuration.add_user(user_id):
        for admin in configuration.get_config("admin", "admins", []):
            bot.send_message(chat_id=admin, text="Added user "+str(user_id)+".")
        bot.send_message(chat_id=user_id, text="You've been granted an access.\nCommands:\n"+core.get_user_commands())

@restricted
def unauth(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if len(args) != 1:
        bot.send_message(chat_id=update.message.chat_id, text="Usage: /unauth userd_id.")
        return
    arg = int(args[0])
    remove_user(bot, arg)

def remove_user(bot, user_id):
    if configuration.remove_user(user_id):
        for admin in configuration.get_config("admin", "admins", []):
            bot.send_message(chat_id=admin, text="Removed user "+str(user_id)+".")
        bot.send_message(chat_id=user_id, text="Your access has been revoked.")

@restricted
def users(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    bot.send_message(chat_id=update.message.chat_id, text=configuration.get_users())


@restricted
def stop(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    button_list = [
        InlineKeyboardButton(text="Yes", callback_data="stop_yes"),
        InlineKeyboardButton(text="No", callback_data="stop_no"),
    ]
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    bot.send_message(update.message.chat_id, "Sure?", reply_markup=reply_markup)

def _stop(bot, query):
    bot.editMessageText(text="Stopping...",
                        chat_id=query.message.chat_id,
                        message_id=query.message.message_id)
    for pid in psutil.pids():
        p = psutil.Process(pid)
        if p.name() == "python" and len(p.cmdline()) > 1 and "/home/pi/pyhome/pyhome.py" in p.cmdline()[1]:
            logger.info("killing {} {} {}".format(pid, p.name(), ' '.join(p.cmdline())))
            subprocess.call(["sudo", "kill", str(pid)])

@restricted
def reboot(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    button_list = [
        InlineKeyboardButton(text="Yes", callback_data="reboot_yes"),
        InlineKeyboardButton(text="No", callback_data="reboot_no"),
    ]
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    bot.send_message(update.message.chat_id, "Sure?", reply_markup=reply_markup)

def _reboot(bot, query):
    bot.editMessageText(text="Rebooting...",
                        chat_id=query.message.chat_id,
                        message_id=query.message.message_id)
    os.system('reboot now')

def button(bot, update):
    query = update.callback_query
    cmd = query.data.split(':')[0]
    if len(query.data.split(':')) > 1:
         data = query.data.split(':')[1]
    else:
         data = '0'
    if cmd == "reboot_yes":
        _reboot(bot, query)
        return
    if cmd == "reboot_no":
        bot.editMessageText(text="Reboot canceled.",
                            chat_id=query.message.chat_id,
                            message_id=query.message.message_id)
        return
    if cmd == "stop_yes":
        _stop(bot, query)
        return
    if cmd == "stop_no":
        bot.editMessageText(text="Stop canceled.",
                            chat_id=query.message.chat_id,
                            message_id=query.message.message_id)
    if cmd == "signin_no":
        bot.editMessageText(text="Query canceled.",
                            chat_id=query.message.chat_id,
                            message_id=query.message.message_id)
        bot.send_message(int(data), "Your sign in request has not been approved.")
    if cmd == "signin_yes":
        bot.editMessageText(text="Query approved.",
                            chat_id=query.message.chat_id,
                            message_id=query.message.message_id)
        add_user(bot, int(data))
    if cmd == "signout_no":
        bot.editMessageText(text="Query canceled.",
                            chat_id=query.message.chat_id,
                            message_id=query.message.message_id)
    if cmd == "signout_yes":
        bot.editMessageText(text="Query approved.",
                            chat_id=query.message.chat_id,
                            message_id=query.message.message_id)
        remove_user(bot, int(data))


def signup(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if update.message.chat_id in configuration.get_config("admin", "users", []):
        bot.send_message(update.message.chat_id, "You are signed in already.")
        return
    button_list = [
        InlineKeyboardButton(text="Yes", callback_data="signin_yes:"+str(update.message.chat_id)),
        InlineKeyboardButton(text="No", callback_data="signin_no:"+str(update.message.chat_id)),
    ]
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    for admin in configuration.get_config("admin", "admins", []):
        bot.send_message(admin, "Signin request from "+str(update.message.chat_id)+". Approve?", reply_markup=reply_markup)

@user
def signout(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    button_list = [
        InlineKeyboardButton(text="Yes", callback_data="signout_yes:"+str(update.message.chat_id)),
        InlineKeyboardButton(text="No", callback_data="signout_no"),
    ]
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    bot.send_message(update.message.chat_id, "Sure?", reply_markup=reply_markup)

@user
def help(bot, update, args):
    if update.message.chat_id in configuration.get_config("admin", "admins", []):
        bot.send_message(update.message.chat_id, core.get_admin_commands())
    else:
        bot.send_message(update.message.chat_id, core.get_user_commands())

@restricted
def get_conf(bot, update, args):
    bot.send_message(update.message.chat_id, json.dumps(configuration.CONFIG, indent=2))

@restricted
def set_conf(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if len(args) != 3:
        bot.send_message(update.message.chat_id, "Usage: /set_conf {group} {param} {value}")
        return
    configuration.set_config(args[0], args[1], args[2])
    bot.send_message(update.message.chat_id, json.dumps(configuration.CONFIG[args[0]], indent=2))


CMD = {"admin":[auth, unauth, users, stop, reboot, set_conf, get_conf, scan],
       "user":[signout, help],
       "all": [signup],
       "button": [button]}
