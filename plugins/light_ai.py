from core import user, restricted
import telegram
import json
import datetime
import threading
import configuration, state
import time
import logging
import RPi.GPIO as GPIO
import random

logger = logging.getLogger("pyhome")

# Define function to measure charge time
def _RCtime (PiPin):
  measurement = 0
  # Discharge capacitor
  GPIO.setup(PiPin, GPIO.OUT)
  GPIO.output(PiPin, GPIO.LOW)
  time.sleep(0.1)

  GPIO.setup(PiPin, GPIO.IN)
  # Count loops until voltage across
  # capacitor reads high on GPIO
  start = time.time()
  while (GPIO.input(PiPin) == GPIO.LOW and time.time() - start <= 5):
    measurement += 1

  return measurement

def initialize(bot):
    if state.get_state("light_ai", False):
        EVENT.clear()
        _start_periodic(900)

@restricted
def light_ai(bot, update, args):
    global EVENT
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if len(args) != 1:
        bot.send_message(chat_id=update.message.chat_id, text="Usage: /light_ai {ON|OFF|on|off}")
        return
    arg = args[0].upper()
    if arg not in ["ON", "OFF"]:
        bot.send_message(chat_id=update.message.chat_id, text="Usage: /light_ai {ON|OFF|on|off}")
        return
    if arg == "ON":
        EVENT.clear()
        _start_periodic(900)
        state.set_state("light_ai", True)
        bot.send_message(chat_id=update.message.chat_id, text="Light AI started.")
        return
    if arg == "OFF":
        EVENT.set()
        state.set_state("light_ai", False)
        bot.send_message(chat_id=update.message.chat_id, text="Light AI stopped.")
        return

def lights_on():
    status = state.get_state("lights", "OFF")
    print status
    if status == "ON":
        return
    GPIO.setmode(GPIO.BOARD)

    ldrc_pin = configuration.get_config("pinout", "LDRC", 19)
    relay_pin = configuration.get_config("pinout", "Relay", 12)

    before = 0
    count = 0
    start = time.time()
    while time.time() - start < 1:
        before += _RCtime(ldrc_pin)
        count += 1
    before = before/count
    print before
    #time.sleep(0.5)
    GPIO.setup(relay_pin, GPIO.OUT)
    time.sleep(0.5)
    GPIO.setup(relay_pin, GPIO.IN)
    #time.sleep(0.5)

    after = 0
    count = 0
    start = time.time()
    while time.time() - start < 1:
        after += _RCtime(ldrc_pin)
        count += 1
    after = after/count
    print after

    df = after - before

    if df > 0:
        state.set_state("lights", "OFF")
    if df < 0:
        state.set_state("lights", "ON")
    if df >= 0:
        lights_on()

def lights_off():
    status = state.get_state("lights", "ON")
    print status
    if status == "OFF":
        return
    GPIO.setmode(GPIO.BOARD)

    ldrc_pin = configuration.get_config("pinout", "LDRC", 19)
    relay_pin = configuration.get_config("pinout", "Relay", 12)

    before = 0
    count = 0
    start = time.time()
    while time.time() - start < 1:
        before += _RCtime(ldrc_pin)
        count += 1
    before = before/count
   
    #time.sleep(0.5)
    GPIO.setup(relay_pin, GPIO.OUT)
    time.sleep(0.5)
    GPIO.setup(relay_pin, GPIO.IN)
    #time.sleep(0.5)

    after = 0
    count = 0
    start = time.time()
    while time.time() - start < 1:
        after += _RCtime(ldrc_pin)
        count += 1
    after = after/count

    df = after - before
    print df

    if df > 0:
        state.set_state("lights", "OFF")
    if df < 0:
        state.set_state("lights", "ON")
    if df <= 0:
        print "lights are not off, trying again"
        lights_off()
    print "lights are off"


def _start_periodic(sec):
    global EVENT

    ldrc_pin = configuration.get_config("pinout", "LDRC", 19)

    avg = 0
    count = 0
    start = time.time()
    while time.time() - start < 1:
        avg += _RCtime(ldrc_pin)
        count += 1
    avg = avg/count

    now = time.time()
    nowdt = datetime.datetime.now()

    eh = 18
    if nowdt.weekday() <= 4:
        eh = 20

    if nowdt.hour >= eh and nowdt.hour < 22 and avg >= 15000:
        status = state.get_state("lights", "OFF")
        if status == "OFF":
            lights_on()
            tout = (22 - nowdt.hour)*60*60 + (0.5 - random.random())*60*60
            print tout
            tsk = threading.Timer(tout,lights_off)
            tsk.setDaemon(True)
            tsk.start()

    if EVENT.isSet():
        return
    t = threading.Timer(sec,_start_periodic, args=(sec,))
    t.setDaemon(True)
    t.start()

EVENT = threading.Event()

CMD = {"admin": [light_ai],
       "user": [],#schedule, unschedule],
       "all": []}
