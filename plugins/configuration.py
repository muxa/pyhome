import json

CONFIG = {}

def load():
    global CONFIG
    with open('/home/pi/pyhome/pyhome.conf', 'r') as data_file:    
        CONFIG = json.load(data_file)
    print "Loaded\n", json.dumps(CONFIG,indent=2)

def write():
    global CONFIG
    with open('/home/pi/pyhome/pyhome.conf', 'w') as data_file:
        json.dump(CONFIG, data_file)

def get_config(section, param, default):
    global CONFIG
    return CONFIG.get(section, {}).get(param, default)

def get_users():
    global CONFIG
    return "["+",".join(str(user) for user in CONFIG["admin"]["users"])+"]"

def set_config(section, param, value):
    global CONFIG
    if section == "admin":
        return
    if section not in CONFIG:
        CONFIG[section] = {}
    try:
        CONFIG[section][param] = int(value)
    except ValueError:
        CONFIG[section][param] = value
    write()

def add_user(uid):
    global CONFIG
    if uid in CONFIG["admin"]["users"]:
        return False
    CONFIG["admin"]["users"].append(uid)
    write()
    return True

def remove_user(uid):
    global CONFIG
    if uid not in CONFIG["admin"]["users"]:
        return False
    CONFIG["admin"]["users"].remove(uid)
    write()
    return True

