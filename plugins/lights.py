import telegram
from core import restricted, user
import RPi.GPIO as GPIO
import time
import threading
import configuration, state


# Define function to measure charge time
def _RCtime (PiPin):
  measurement = 0
  # Discharge capacitor
  GPIO.setup(PiPin, GPIO.OUT)
  GPIO.output(PiPin, GPIO.LOW)
  time.sleep(0.1)

  GPIO.setup(PiPin, GPIO.IN)
  # Count loops until voltage across
  # capacitor reads high on GPIO
  start = time.time()
  while (GPIO.input(PiPin) == GPIO.LOW and time.time() - start < 20):
    measurement += 1

  return measurement

@user
def light(bot, update, args):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    GPIO.setmode(GPIO.BOARD)
    
    ldrc_pin = configuration.get_config("pinout", "LDRC", 19)
    relay_pin = configuration.get_config("pinout", "Relay", 12)

    before = 0
    count = 0
    start = time.time()
    while time.time() - start < 1:
        before += _RCtime(ldrc_pin)
        count += 1
    before = before/count    

    #time.sleep(0.5)
    GPIO.setup(relay_pin, GPIO.OUT)
    time.sleep(0.5)
    GPIO.setup(relay_pin, GPIO.IN)
    #time.sleep(0.5)

    after = 0
    count = 0
    start = time.time()
    while time.time() - start < 1:
        after += _RCtime(ldrc_pin)
        count += 1
    after = after/count

    df = after - before
    status = "in an unknown status"
    if df > 0:
        status = "OFF"
    if df < 0:
        status = "ON"
    state.set_state("lights", status)
    #stop_event.set()
    bot.send_message(chat_id=update.message.chat_id, text="Light is "+status+". ("+str(df)+")")


CMD = {"admin": [],
       "user": [light],
       "all": []}
