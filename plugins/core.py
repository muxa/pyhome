from functools import wraps
import logging
import configuration
import telegram
import subprocess
import time, os, sys
import psutil

from os import listdir
from os.path import dirname, basename, isfile, join
import glob
from importlib import import_module

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import CallbackQueryHandler

import state

logger = logging.getLogger('pyhome')

CMDS = {"admin": [],
        "user": [],
        "all": []}

updater = None

def init():
    global CMDS, updater
    configuration.load()
    state.load()    

    token = configuration.get_config("token", "telegram", "")
    updater = Updater(token=token)
    dispatcher = updater.dispatcher
    bot = updater.bot
 
    scan_plugins(bot)
    for admin in configuration.get_config("admin", "admins", []):
        bot.sendMessage(chat_id=admin, text="Bot is up and running. Loaded commands:\n"+get_admin_commands())

    updater.start_polling()
    updater.idle()

def scan_plugins(bot):
    global CMDS, updater
    old_cmds = CMDS["admin"] + CMDS["user"] + CMDS["all"]
    new_cmds = []
    CMDS = {"admin": [],
            "user": [],
            "all": []}
    dispatcher = updater.dispatcher
    modules = glob.glob(dirname(__file__)+"/*.py")
    all = [ basename(f)[:-3] for f in modules if isfile(f)]
    for module_name in all:
        if "__init__" in module_name:
            continue
        module = import_module('plugins.'+module_name)
        if not hasattr(module, 'CMD'):
            continue
        if hasattr(module, "initialize"):
            module.initialize(bot)
        attr = module.CMD
        for g in attr:
            for f in attr[g]:
                if g == "button":
                    dispatcher.add_handler(CallbackQueryHandler(f))
                else:
                    if f not in old_cmds:
                        new_cmds.append('/'+f.__name__)
                    dispatcher.add_handler(CommandHandler(f.__name__,f,pass_args=True))
                    CMDS[g].append(f)

    return '\n'.join(new_cmds)

def get_admin_commands():
    global CMDS
    cmds = []
    for g in ["admin", "user", "all"]:
        for f in CMDS[g]:
            cmds.append('/'+f.__name__)
    return '\n'.join(cmds)

def get_user_commands():
    global CMDS
    cmds = []
    for g in ["user", "all"]:  
        for f in CMDS[g]:
            cmds.append('/'+f.__name__)
    return '\n'.join(cmds)

def get_other_commands():
    global CMDS
    cmds = []
    for g in ["all"]:
        for f in CMDS[g]:
            cmds.append('/'+f.__name__)
    return '\n'.join(cmds)

def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu

def restricted(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        # extract user_id from arbitrary update
        try:
            user_id = update.message.from_user.id
        except (NameError, AttributeError):
            try:
                user_id = update.inline_query.from_user.id
            except (NameError, AttributeError):
                try:
                    user_id = update.chosen_inline_result.from_user.id
                except (NameError, AttributeError):
                    try:
                        user_id = update.callback_query.from_user.id
                    except (NameError, AttributeError):
                        logger.info("No user_id available in update.")
                        return
        if user_id not in configuration.get_config("admin", "admins", []):
            logger.info("Unauthorized access denied for {}.".format(user_id))
            for admin in configuration.get_config("admin", "admins", []):
                bot.send_message(chat_id=admin, text="Unauthorized access denied for {}.".format(user_id))
            return
        return func(bot, update, *args, **kwargs)
    return wrapped


def user(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        # extract user_id from arbitrary update
        try:
            user_id = update.message.from_user.id
        except (NameError, AttributeError):
            try:
                user_id = update.inline_query.from_user.id
            except (NameError, AttributeError):
                try:
                    user_id = update.chosen_inline_result.from_user.id
                except (NameError, AttributeError):
                    try:
                        user_id = update.callback_query.from_user.id
                    except (NameError, AttributeError):
                        logger.info("No user_id available in update.")
                        return
        if user_id not in configuration.get_config("admin", "users", [])+configuration.get_config("admin", "admins", []):
            logger.info("Unauthorized access denied for {}.".format(user_id))
            for admin in configuration.get_config("admin", "admins", []):
                bot.send_message(chat_id=admin, text="Unauthorized access denied for {}.".format(user_id))
            return
        return func(bot, update, *args, **kwargs)
    return wrapped

