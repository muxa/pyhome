import json

STATE = {}

def load():
    global STATE
    with open('/home/pi/pyhome/plugins/state.conf', 'r') as data_file:    
        STATE = json.load(data_file)
    print "Loaded state\n", json.dumps(STATE,indent=2)

def write():
    global STATE
    with open('/home/pi/pyhome/plugins/state.conf', 'w') as data_file:
        json.dump(STATE, data_file)

def set_state(name, value):
    global STATE
    STATE[name] = value
    write()

def get_state(name, default):
    global STATE
    return STATE.get(name, default)
