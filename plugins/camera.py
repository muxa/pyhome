import time
import threading
import picamera
import datetime as dt
import io
import subprocess
import RPi.GPIO as GPIO
import io
import picamera
import logging
import SocketServer as socketserver
from threading import Condition
import BaseHTTPServer as server
from executor import Command
import configuration, state

def generate_page():
    return """<html>
<head>
  <title>picamera MJPEG streaming demo</title>
</head>
<body>
  <h1>PiCamera MJPEG Streaming Demo</h1>
  <iframe width="0" height="0" border="0" name="dummyframe" id="dummyframe"></iframe><br>
  <img src="stream.mjpg" width='"""+str(configuration.get_config("stream", "width", 640))+"""' height='"""+str(configuration.get_config("stream", "height", 360))+"""' /><br>
  <form action="left" method="post" target="dummyframe" style="display:inline">
    <input type="submit" value="<=LEFT" style="width:211px; height:100">
  </form>
  <form action="center" method="post" target="dummyframe" style="display:inline">
    <input type="submit" value="CENTER" style="width:211px; height:100">
  </form>
  <form action="right" method="post" target="dummyframe" style="display:inline">
    <input type="submit" value="RIGHT=>" style="width:211; height:100">
  </form><br>
</body>
</html>"""

class StreamingOutput(object):

    def __init__(self):
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = Condition()
        return

    def write(self, buf):
        if buf.startswith('\xff\xd8'):
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)


class StreamingHandler(server.BaseHTTPRequestHandler):
    angle = 90

    def do_POST(self):
        angle = state.get_state("angle", 90)
        if self.path == '/right':
            if angle < 0:
                return
            angle -= 15
        elif self.path == '/left':
            if angle > 190:
                return
            angle += 15
        elif self.path == '/center':
            angle = 90
        state.set_state("angle", angle)
        GPIO.setmode(GPIO.BOARD)
        servo_pin = configuration.get_config("pinout", "Servo", 16)
        GPIO.setup(servo_pin, GPIO.OUT)
        pwm = GPIO.PWM(servo_pin, 50)
        pwm.start(0)
        pwm.ChangeDutyCycle(angle / 18 + 1)
        time.sleep(0.3)

    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/index.html':
            content = generate_page().encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with SurveillanceCamera.output.condition:
                        SurveillanceCamera.output.condition.wait()
                        frame = SurveillanceCamera.output.frame
                    self.wfile.write('--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write('\r\n')

            except Exception as e:
                logging.warning('Removed streaming client %s: %s', self.client_address, str(e))

        else:
            self.send_error(404)
            self.end_headers()


class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True


def write_video(stream, name):
    with io.open(name + '-before.h264', 'wb') as output:
        for frame in stream.frames:
            if frame.frame_type == picamera.PiVideoFrameType.sps_header:
                stream.seek(frame.position)
                break

        while True:
            buf = stream.read1()
            if not buf:
                break
            output.write(buf)

    stream.seek(0)
    stream.truncate()


class SurveillanceCamera:
    motion_event = threading.Event()
    thread = None
    stop_event = None
    security_event = threading.Event()
    stream_event = threading.Event()
    camera = picamera.PiCamera()
    preseconds = 5
    filepath = None
    stream_thread = None
    frame = None
    output = StreamingOutput()
    output_pic = StreamingOutput()
    queue = None
    server = None

    def initialize(self, **config):
        width = configuration.get_config("security", "width", 1280)
        height = configuration.get_config("security", "height", 720)
        fps = configuration.get_config("camera", "fps", 32)
        rotation = configuration.get_config("camera", "rotation", 180)

        # need to initialize servo here for more stable position
        servo_pin = configuration.get_config("pinout", "Servo", 16)
        angle = state.get_state("angle", 90)

        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(servo_pin, GPIO.OUT)

        pwm = GPIO.PWM(servo_pin, 50)
        pwm.start(0)
        pwm.ChangeDutyCycle(angle / 18 + 1)
        time.sleep(0.5)

        SurveillanceCamera.camera.resolution = (width, height)
        SurveillanceCamera.camera.framerate = fps
        SurveillanceCamera.camera.rotation = rotation
        SurveillanceCamera.camera.video_stabilization = True
        SurveillanceCamera.camera.annotate_background = True
        
        SurveillanceCamera.output_pic = StreamingOutput()
        SurveillanceCamera.camera.start_recording(SurveillanceCamera.output_pic, format='mjpeg', resize=(width, height), splitter_port=3)

    def start_security(self):
        if SurveillanceCamera.thread is None:
            SurveillanceCamera.security_event.clear()
            SurveillanceCamera.thread = threading.Thread(target=self._worker)
            SurveillanceCamera.thread.daemon = True
            SurveillanceCamera.thread.start()
            state.set_state("security", True)

    def start_stream(self):
        if SurveillanceCamera.stream_thread is None:
            SurveillanceCamera.stream_event.clear()
            SurveillanceCamera.stream_thread = threading.Thread(target=self._streamer)
            SurveillanceCamera.stream_thread.daemon = True
            SurveillanceCamera.stream_thread.start()
            state.set_state("stream", True)
    
    def stop_security(self):
        SurveillanceCamera.security_event.set()
        state.set_state("security", False)

    def stop_stream(self):
        SurveillanceCamera.server.shutdown()
        SurveillanceCamera.server.socket.close()
        SurveillanceCamera.server = None
        state.set_state("stream", False)

    def set_event(self, event):
        pass

    def set_command_queue(self, queue):
        SurveillanceCamera.queue = queue

    def set_stop_event(self, stop_event):
        SurveillanceCamera.stop_event = stop_event

    def set_event_trigger(self, trigger):
        trigger.set_event(SurveillanceCamera.motion_event)

    @classmethod
    def _streamer(cls):
        stream_w = configuration.get_config("stream", "width", 640)
        stream_h = configuration.get_config("stream", "height", 360)
        cls.output = StreamingOutput()
        cls.camera.start_recording(cls.output, format='mjpeg', resize=(stream_w, stream_h), splitter_port=2)
        try:
            address = ('', 8000)
            cls.server = StreamingServer(address, StreamingHandler)
            cls.server.serve_forever()
        finally:
            cls.camera.stop_recording(splitter_port=2)
            cls.stream_thread = None

    @classmethod
    def _worker(cls):
        preseconds = configuration.get_config("security", "preseconds", 5)
        filepath = configuration.get_config("security", "path", "/home/pi/motion/video")
        
        pir_pin = configuration.get_config("pinout", "PIR", 15)

        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(pir_pin, GPIO.IN)
        
        stream = picamera.PiCameraCircularIO(cls.camera, seconds=preseconds)
        cls.camera.start_recording(stream, format='h264', splitter_port=1)
        while not cls.security_event.isSet():
            cls.camera.annotate_text = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            i = GPIO.input(pir_pin)
            if i == 1:
                motion_time = dt.datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
                print 'Motion detected: ', motion_time
                motion_filename = filepath + '/' + motion_time
                cls.camera.split_recording(motion_filename + '-after.h264', splitter_port=1)
                write_video(stream, motion_filename)
                while i == 1:
                    cls.camera.annotate_text = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    cls.camera.wait_recording(1, splitter_port=1)
                    i = GPIO.input(pir_pin)
                cls.camera.split_recording(stream, splitter_port=1)
                if cls.queue is not None:
                    cls.queue.put(Command(type='video', args=(motion_filename + '-before.h264', motion_filename + '-after.h264', motion_time)))
                print 'Motion stopped:', dt.datetime.now()
            cls.camera.wait_recording(1, splitter_port=1)

        cls.thread = None
        cls.camera.stop_recording(splitter_port=1)
        return
