TOKEN='313213137:AAHtV09ElwMtKv_O7UR4RGWXCWwR_mJJOEI'

LIST_OF_ADMINS = [93833603]

from functools import wraps
import logging
LOG_FILENAME='pyhome.log'
logging.basicConfig(filename=LOG_FILENAME, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

logger = logging.getLogger('pyhome')

def restricted(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        # extract user_id from arbitrary update
        try:
            user_id = update.message.from_user.id
        except (NameError, AttributeError):
            try:
                user_id = update.inline_query.from_user.id
            except (NameError, AttributeError):
                try:
                    user_id = update.chosen_inline_result.from_user.id
                except (NameError, AttributeError):
                    try:
                        user_id = update.callback_query.from_user.id
                    except (NameError, AttributeError):
                        logger.info("No user_id available in update.")
                        return
        if user_id not in LIST_OF_ADMINS:
            logger.info("Unauthorized access denied for {}.".format(user_id))
            return
        return func(bot, update, *args, **kwargs)
    return wrapped

from telegram.ext import Updater

updater = Updater(token=TOKEN)

dispatcher = updater.dispatcher

from telegram.ext import CommandHandler

@restricted
def rping(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="rpong")

def ping(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="pong")

def args(bot, update, args):
    text_caps = ' '.join(args).upper()
    bot.sendMessage(chat_id=update.message.chat_id, text=text_caps)

args_handler = CommandHandler('args', args, pass_args=True)

dispatcher.add_handler(args_handler)

ping_handler = CommandHandler('ping', ping)
rping_handler = CommandHandler('rping', rping)

dispatcher.add_handler(ping_handler)
dispatcher.add_handler(rping_handler)

updater.start_polling()
updater.idle()
