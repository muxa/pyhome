# PyHome #

**PyHome** is a light-weight DIY smart-home project built around Raspberry PI, RasPiCam (NoIR), and a bunch of sensors. The features are (with a current state in brackets):

* Video surveillance with http streaming, motion detection, and alarm video capture
* Manual ceiling lights control
* Automatic ceiling lights control depending on time and light intensity
* 433Hz devices control; for example, ceiling lights, heaters, etc. (tests)
* Telegram binding
* Dropbox binding

It is a highly modular DIY solution built on a tight software-hardware interaction. It directly operates with a peripheral hardware, while providing a high-level command based front-end to users via Telegram commands and Dropbox binding.

## Architecture overview

The software is built around [Python Telegram Bot](https://github.com/python-telegram-bot/python-telegram-bot). Whenever started, PyHome scans a ```plugins``` folder for plugins and fetches the commands from the found plugins. Those commands are dynamically bound with the corresponding functions via [Python Telegram Bot](https://github.com/python-telegram-bot/python-telegram-bot) binding routine.

The software operates in four threads:

 * Telegram bot thread; it listens for commands and calls the corresponding functions from plugins.
 * Camera surveillance thread; can optionally be launched, periodically checks the readings for Presence InfraRed (PIR) sensor, captures a High-Resolution (HR) video in case of motion detection, and passes the file to the Executor thread.
 * Executor thread; compresses the HR-video via ```ffmpeg``` utility, sends the compressed video via Telegram, and send the HR-video to Dropbox.
 * Video stream thread; works as a basic http daemon, feeds the web page with an image from a camera buffer.

## Configuration

The configuration file contains the following:

 * General camera settings as frame ```rotation``` and ```FPS```.
 * Camera streaming parameters as ```width``` and ```height```.
 * Security parameters such as:
    * Seconds captured before motion event --- ```preseconds```
    * Capture ```width``` and ```height```; the captured file will be sent to Dropbox uncompressed.
    * ```path``` where the uncompressed file will be stored before sending to Dropbox.
 * Tokens for Dropbox and Telegram bindings.
 * Pinout for the peripheral hardware.
 * Lists of telegram ids of users and admins.

## Functionality Extension 

To create your own plugin, you must use the following template:

```
#!python

from core import restricted, user

# this function is only available to admins
@restricted
def rping(bot, update, args):
    bot.sendMessage(chat_id=update.message.chat_id, text="rpong")

# this function available to users and admins
@user
def ping(bot, update, args):
    bot.sendMessage(chat_id=update.message.chat_id, text="pong")

# this function will be available to everyone once reported in CMD["all"]
def args(bot, update, args):
    text_caps = ' '.join(args).upper()
    bot.sendMessage(chat_id=update.message.chat_id, text=text_caps)

CMD = {"admin": [rping],  # this list will be reported to admins
       "user": [ping],  # this list will be reported to admins and users
       "all": []}  # this list will be reported to admins, users, and everyone else

```

Whenever PyHome is started, it scans ```plugins``` folder and looks for ```CMD``` member of each python module in the folder. The ```CMD``` is used to bind the commands name to the function names via [Python Telegram Bot](https://github.com/python-telegram-bot/python-telegram-bot). Thus, every function added to ```CMD``` will be available in Telegram bot as a command. Each list in ```CMD``` used to form a list of commands that is send via ```/help``` command. For example, should admin send the latter command, he/she will receive all three lists; users will receive commands from "user" and "all" lists; everyone else will receive "all" list.

The actual access control is made with the ```@restricted``` or ```@user``` decorators, but separation in ```CMD``` is made solely for convenience issues. Indeed, there are no reasons to report users the commands they have no access to, and the info in ```CMD``` is used to separate the output of the ```/help``` command.

## Available plugins

### Admin

Allows users to request an access to functions with ```@user``` decorator. Allows administrators to accept or reject the request, or to silently subscribe anyone to bot updates; for example to motion detection events. Also provides tools for bot management: stop, reboot, and config management.

The supported commands:

 * ```/scan``` scans a ```plugins``` folder at run-time, finds, binds, and reports new commands added to bot (only for admins).
 * ```/auth {user_id}```, ```/unauth {user_id}``` authorise/remove user (only for admins).
 * ```/users``` prints all the registered users (only for admins).
 * ```/stop``` stops the bot (only for admins).
 * ```/reboot``` reboots the RaspberryPi (only for admins).
 * ```/signup``` request access.
 * ```/signout``` self-revoke access (only for users).
 * ```/help``` get list of commands  (only for admins and users).
 * ```/get_conf```, ```/set_conf {GROUP} {PARAM} {VALE}``` configuration management (only for admins).

### Camera Surveillance and Video Streaming

This is the heaviest module. The module initializes a circular buffer with ```preseconds``` capacity in a separate thread. Periodically it checks the PIR sensor. Should the latter detect the motion, the module splits the video port to the file and saves the current state of a buffer. After the motion has stopped, the two files are handed over the Executor thread.

In a different thread, the module starts an HTTP daemon. The latter uses another camera port to feed the image to a webpage. The webpage also contains buttons that allow users to rotate camera using Servo Motor.

The supported commands:

 * ```/secutiry {ON|OFF}``` starts/stops Camera Surveillance thread (only for admins).
 * ```/stream {ON|OFF}``` starts/stops Video Streaming thread (only for admins).
 * ```/pic``` takes and shows an instant image from the camera (only for admins and users).
 * ```/right```, ```/left```, ```/center``` rotates the camera horizontally, takes and shows an instant picture after rotation (only for admins and users).

### Lights and LDR

Current implementation simply uses a relay switch attached to the light switch button. The LDRC circuit allows one to measure the light intensity and to report whether the lights are on or off. Automatically switch ceiling lights on whenever the intensity is low.

The supported commands:

 * ```/light``` toggles the ceiling light (it toggles a switch relay though); reports the change in the capacitor charging time that is inversely proportional to the light intensity.
 * ```/light_ai {ON|OFF}``` enables/disables Light AI. The latter enables light when it's evening (6-9pm) and low light intensity; disables light when it's 10pm+/-30mins (randomly).

### 433Hz Rx/Tx

The 433Hz radio modules can be connected to a reserved pins, however, there is no functionality yet. The simple tests are intended to check the correct behavior and to experiment with the possible usages of the modules.

# Hardware

The project is compatible with any Raspberry Pi model, but RaspberryPi 3 is strongly recommended primarily because of native support of 4 distinct processes.

[This pinout (click me)](https://bitbucket.org/repo/z888GGj/images/3722784910-pinout.png) is currently used for the existing plugins, however, it can be modified via the config file.

### Camera

Any camera can be used, for example, the one on the image below:

![41Q43guqtVL 500x500.jpg](https://bitbucket.org/repo/z888GGj/images/1945333243-41Q43guqtVL%20500x500.jpg)

### Servo Motor

A servomotor is used to rotate the camera. While all the servo motors are compatible, in this project is used the cheapest one.

![41CTpuGELfL500x500.jpg](https://bitbucket.org/repo/z888GGj/images/4112680814-41CTpuGELfL500x500.jpg)

This servo is PWM operated, thus we need to adjust a duty cycle to be able to rotate it to a certain angle. The code below rotates the motor the input of which is connected to pin 16 in RPI BOARD layout. One may want to play with the code to find the optimal duty cycle parameters.

```
#!python

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(16, GPIO.OUT)
pwm = GPIO.PWM(16, 50)
pwm.start(0)
da = 90
while da <= 190:
    pwm.ChangeDutyCycle(da/18 + 1)
    da = da + 10
    time.sleep(0.5)
while da >= 0:
    pwm.ChangeDutyCycle(da/18 + 1)
    da = da - 10
    time.sleep(0.5)
while da <= 90:
    pwm.ChangeDutyCycle(da/18 + 1)
    da = da + 10
    time.sleep(0.5)
```

## Motion detection (PIR sensor)

The motion is detected by the dedicated sensor. The PIR sensor has been selected as the video-based motion detection is unreliable and has a lot of false positives.

![71x6v5df8tL._SL1500_1024x367.640x229.jpg](https://bitbucket.org/repo/z888GGj/images/827594558-71x6v5df8tL._SL1500_1024x367.640x229.jpg)

The installation is trivial and the following code shows how one can use the sensor attached to the pin 16 in RPI BOARD layout.


```
#!python

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(16, GPIO.OUT)

start = time.time()
while time.time() - start < 60:
    i = GPIO.input(pir_pin)
    if i == 1:
        print 'Motion detected'
        while i == 1:
            i = GPIO.input(pir_pin)
        print 'Motion stopped'
```

## Relay (ceiling lights)

In the current solution, the relay is connected to the ceiling light remote control. The relay emulates the button press, and the remote control sends a signal to the ceiling lights.

![713T1EaqphL._SL1200_ 640x640.jpg](https://bitbucket.org/repo/z888GGj/images/566548803-713T1EaqphL._SL1200_%20640x640.jpg)

In the following code, the relay is attached to pin 12 in RPI BOARD layout. The code changes the state of the relay 4 times every 2 seconds. Thus, it emulates the button press for 2 seconds on the remote, then the button is released for 2 seconds, and then pressed again for 2 seconds. It results in switching the lights on and then off.

```
#!python

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

start = time.time()
while time.time() - start < 8:
    GPIO.setup(12, GPIO.OUT)
    time.sleep(2)
    GPIO.setup(12, GPIO.IN)
    time.sleep(2)
```

## Light intensity measurement (LDR-C circuit)

This solution uses a trick to measure light intensity. The input pins of RaspberryPi are digital and basically, can not measure the resistance of the light-depended resistor (LDR). To remedy this issue, one can make an LDR-C circuit by serially connecting a capacitor and the LDR, as on the image below. When we apply 3.3V as on the image for a couple of milliseconds, the capacitor charges; when it's charge is more than 2.2V, the RaspberryPi will measure ```1``` on the ```OUT``` pin shown in the picture.

![](https://bitbucket.org/repo/z888GGj/images/3887935630-Microsoft%20Visio%20Drawing.png)

The charging time depends on the LDR resistivity - the higher resistivity leads to the slower capacitor charge. The resistivity depends on the light intensity - the lower light intensity implies the higher resistivity. Thus, the low light intensity leads to the slower capacitor charge. Keeping this in mind, we can measure the time needed for a capacitor to charge and thus to have an idea of the light intensity.

```
#!python

import RPi.GPIO as GPIO
import time

# Define function to measure charge time
def _RCtime (PiPin):
  measurement = 0
  # Discharge capacitor
  GPIO.setup(PiPin, GPIO.OUT)
  GPIO.output(PiPin, GPIO.LOW)
  time.sleep(0.1)

  GPIO.setup(PiPin, GPIO.IN)
  # Count loops until voltage across
  # capacitor reads high on GPIO
  while (GPIO.input(PiPin) == GPIO.LOW):
    measurement += 1

  return measurement

```