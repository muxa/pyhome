import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

start = time.time()
while time.time() - start < 8:
    GPIO.setup(12, GPIO.OUT)
    time.sleep(2)
    GPIO.setup(12, GPIO.IN)
    time.sleep(2)
