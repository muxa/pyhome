import psutil
import sys

import logging

from plugins import core

LOG_FILENAME='pyhome.log'
logging.basicConfig(filename=LOG_FILENAME, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

logger = logging.getLogger('pyhome')

if __name__ == "__main__":
    count = 0
    for pid in psutil.pids():
        p = psutil.Process(pid)
        if p.name() == "python" and len(p.cmdline()) > 1 and "/home/pi/pyhome/pyhome.py" in p.cmdline()[1]:
            count += 1
    # if there are two processes, then top this one
    if count >= 2:
        sys.exit(0)

    core.init()
