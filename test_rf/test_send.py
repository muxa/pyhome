import time
import sys
import RPi.GPIO as GPIO

sleep = 0.000001

TRANSMIT_PIN = 11

def transmit_code(code):
    '''Transmit a chosen code string using the GPIO transmitter'''
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(TRANSMIT_PIN, GPIO.OUT)
    for i in code:
        if i == '1':
            GPIO.output(TRANSMIT_PIN, 1)
        elif i == '0':
            GPIO.output(TRANSMIT_PIN, 0)
        time.sleep(sleep)
    GPIO.output(TRANSMIT_PIN, 0)
    GPIO.cleanup()

if __name__ == '__main__':
    f = open('signal', 'r')
    code = f.readline()
    while True:
        transmit_code(code)


