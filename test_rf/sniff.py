from datetime import datetime
import matplotlib.pyplot as pyplot
import RPi.GPIO as GPIO
import time

MAX_DURATION = 5
sleep = 0.000001
RECEIVE_PIN = 13

if __name__ == '__main__':
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(RECEIVE_PIN, GPIO.IN)
    f = open('signal', 'w')
    start = time.time()
    print '**Started recording**'
    while time.time() - start <= MAX_DURATION:
        f.write(str(GPIO.input(RECEIVE_PIN)))
        time.sleep(sleep)
    print '**Ended recording**'
    f.close()
