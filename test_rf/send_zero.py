import time
import sys
import RPi.GPIO as GPIO

TRANSMIT_PIN = 11

if __name__ == '__main__':
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(TRANSMIT_PIN, GPIO.OUT)
    while True:
        GPIO.output(TRANSMIT_PIN, 0)

