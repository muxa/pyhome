import RPi.GPIO as GPIO
import time


GPIO.setmode(GPIO.BOARD)
GPIO.setup(16, GPIO.OUT)
pwm = GPIO.PWM(16, 50)
pwm.start(0)
da = 90
while da <= 190:
    pwm.ChangeDutyCycle(da/18 + 1)
    da = da + 10
    time.sleep(0.5)
while da >= 0:
    pwm.ChangeDutyCycle(da/18 + 1)
    da = da - 10
    time.sleep(0.5)
while da <= 90:
    pwm.ChangeDutyCycle(da/18 + 1)
    da = da + 10
    time.sleep(0.5)
