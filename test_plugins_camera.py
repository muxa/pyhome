from plugins import camera

import threading
import time

class TestTrigger:

    def __init__(self):
        self.stop_event = None
        
    def set_event(self, event):
        self.event = event

    def trigger(self):
        if self.event is not None:
            self.event.set()
            time.sleep(5)
            self.event.clear()

if __name__ == "__main__":
    print "STARTED"
    cam = camera.SurveillanceCamera()

    stop_event = threading.Event()
    cam.set_stop_event(stop_event)
    
    trigger = TestTrigger()
    cam.set_event_trigger(trigger)
 
    cam.initialize()
    while True:
        time.sleep(100)
    time.sleep(10)
    trigger.trigger()
    time.sleep(10)
    
    stop_event.set()
